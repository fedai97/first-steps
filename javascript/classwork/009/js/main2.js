document.addEventListener('DOMContentLoaded', mainFunction);

function mainFunction(){
    const input = document.getElementById('keyInput');
    const newSpan = document.getElementById('inputText');

    input.addEventListener('keyup', function(){
        newSpan.innerHTML = "";
        newSpan.innerHTML = this.value;
    });
}