let num = 5.32;
console.log(num.toFixed(1));

//****************************

let fruit = 'banana';
let result = null;

switch (fruit) {
    case 'apple':
        result = 'Apple';
        break;
    case 'pie':
        result = 'Pie';
        break;
    case 'banana':
        result = 'Banana';
        break;
    default:
        result = 'NOTHING';
}

console.log('You have "' + result + '"');

//****************************************
