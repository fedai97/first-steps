const request = new XMLHttpRequest();
const scripts = document.getElementsByTagName("script");
const path = scripts[scripts.length-1].src.slice(0, scripts[scripts.length-1].src.length-10);


console.log(path);
request.open("GET", path + "testjson.json");
request.responseType = 'json';
request.onload = responseLoad;
request.send();

function responseLoad() {
    if (request.readyState === 4) {
        const status = request.status;
        if (status === 200) {
            const resData = request.response;
            const sortedData = sortJSONFile(resData);
            dataDisplayToHTML(sortedData);
        } else {
            console.log("Server res: " + request.statusText);
        }
    }
}

function sortJSONFile(jsonObj) {
    return jsonObj.sort(function(a, b) {
        if (a.nodetype === b.nodetype) {
            return b.nodename - a.nodename;
        }
        return a.nodetype > b.nodetype ? 1 : -1;
    });
}

function dataDisplayToHTML(dataObj) {
    const mainDiv = document.getElementById('main-div');
    const dataUl = document.createElement('ul');
    dataObj.forEach(item => {
        const dataLi = document.createElement('li');
        dataLi.innerHTML = `"nodetype": ${item.nodetype}</br>
                              "nodename": ${item.nodename}</br>
                              "datetime": ${item.datetime}</br></br>`;
        dataUl.appendChild(dataLi);
    });
    mainDiv.appendChild(dataUl);
}

window.addEventListener("load", function () {
    function sendData() {
        const XHR = new XMLHttpRequest();
        const FD = new FormData(form);

        XHR.addEventListener("load", function(event) {
            alert(event.target.responseText);
        });

        XHR.addEventListener("error", function(event) {
            alert('Oops! Something went wrong.');
        });

        XHR.open("POST", path + "?");
        XHR.send(FD);
    }

    const form = document.getElementById("main-form");
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        sendData();
    });
});