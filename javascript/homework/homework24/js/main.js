/**
 * Added number 1 or number 2 to calculator.
 * @param {object} input
 * @param {object} data
 */
function addedNumToCalculator(input, data) {
    if(calculatorLog.operation === ''){
        calculatorLog.num1 += data.value;
        input.value += data.value;
    }else{
        calculatorLog.num2 += data.value;
        input.value += data.value;
    }
    mrcPressed = false;
}

/**
 * Result calculation
 * @param {object} input
 * @param {object} data
 */
function resultCalculator(input, data) {
    let result = calc(calculatorLog);
    calculatorLog.num1 = result;
    calculatorLog.num2 = '';
    if(checkOperation(data.value)){
        calculatorLog.operation = data.value;
        result += data.value;
    }else {
        calculatorLog.operation = '';
    }
    input.value = result;
    mrcPressed = false;
}

/**
 * Definitions operation of calculator.
 * @param {object} input
 * @param {object} data
 */
function definOperationCalculator(input, data) {
    calculatorLog.operation = data.value;
    input.value += data.value;
    mrcPressed = false;
}

/**
 * Delete value of calculator.
 * @param {object} input
 */
function deleteValCalculator(input) {
    calculatorLog.num1 = '';
    calculatorLog.num2 = '';
    calculatorLog.operation = '';
    input.value = '';
    mrcPressed = false;
}

/**
 * Operation with memory of calculator.
 * @param {object} input
 * @param {object} data
 */
function operationWithMemoryCalculator(input, data) {
    if(data.value === 'm+'){
        calculatorLog.result = sum(calculatorLog.result, calculatorLog.num1);
    }else {
        calculatorLog.result = minus(calculatorLog.result, calculatorLog.num1);
    }
    calculatorLog.num1 = '';
    input.value = 'm';
    mrcPressed = false;
}

/**
 * Delete memory of calculator.
 * @param {object} input
 * @param {object} data
 */
function deleteMemoryCalculator(input, data) {
    if(mrcPressed){
        calculatorLog.result = 0;
        input.value = calculatorLog.result;
        mrcPressed = false;
    }else {
        input.value = calculatorLog.result;
        mrcPressed = true;
    }
}

/**
 * Definition of pressed buttons.
 * @param {object} data
 */
function getBtnVal(data){
    const newInput = document.getElementById('display');
    if(+data.value >= 0 || data.value === '.'){
        addedNumToCalculator(newInput, data);
    }else if((data.value === '=' && (calculatorLog.num2 > 0 || calculatorLog.num2.length > 0)) || (checkOperation(data.value) && calculatorLog.num2.length > 0)){
        resultCalculator(newInput, data);
    }else if(checkOperation(data.value) && (calculatorLog.num1 > 0 || calculatorLog.num1.length > 0)){
        definOperationCalculator(newInput, data);
    }else if(data.value === 'C'){
        deleteValCalculator(newInput);
    }else if((data.value === 'm+' || data.value === 'm-') && calculatorLog.operation === ''){
        operationWithMemoryCalculator(newInput, data);
    }else if(data.value === 'mrc'){
        deleteMemoryCalculator(newInput, data);
    }
}

/**
 * @desc Summation function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function sum(value1, value2){
    return +value1 + +value2;
}
/**
 * @desc Subtract function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function minus(value1, value2){
    return +value1 - +value2;
}
/**
 * @desc Multiplication function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function multiply(value1, value2){
    return +value1 * +value2;
}
/**
 * @desc Division function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function division(value1, value2){
    return +value1 / +value2;
}

/**
 * Calculation.
 * @param {object} data
 * @return {number}
 */
function calc(data) {
    switch (data.operation) {
        case '+':
            return sum(data.num1, data.num2);
        case '-':
            return minus(data.num1, data.num2);
        case '*':
            return multiply(data.num1, data.num2);
        case '/':
            return division(data.num1, data.num2);
    }
}

/**
 * Operation check.
 * @param {string} value
 * @return {boolean}
 */
function checkOperation(value) {
    switch (value) {
        case '+':
        case '-':
        case '*':
        case '/':
            return true;
        default:
            return false;
    }
}

const calculatorLog = {
    num1: '',
    num2: '',
    operation: '',
    result: 0
};
let mrcPressed = false;
