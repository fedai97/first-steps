document.addEventListener('keypress', (event) => {
    const keyName = event.key;
    let listBtn = document.getElementsByClassName('btn');
    for(let i = 0; i < listBtn.length; i++){
        listBtn[i].className = 'btn';
        if(listBtn[i].innerHTML.toLowerCase() === keyName.toLowerCase()){
            listBtn[i].className = 'btn active';
        }
    }
});