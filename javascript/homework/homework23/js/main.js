function cl(id){
    const x = document.getElementById(id);
    return x ? (x.className.indexOf('bomb') !== -1 ? 1 : 0) : 0;
}
/**
 * Start the game:
 *  - fill the field with randomly bombs,
 *  - setting handlers to click on the field
 * @param {number} count
 * @param {number} fields
 */
function startGame(count, fields) {
    let bombs = 0;
    let flag = 0;
    let newGame = false;
    for(let i = 0; i < fields; i++){
        const r = document.createElement('div');
        if(Math.random() * fields < count){
            r.className = 'bomb close';
            document.getElementById('text').innerHTML =  flag + '/' + (++bombs);
        }
        else r.className = 'close';
        r.id = Math.floor(i / 10) + '_' + i % 10;
        document.body.appendChild(r);
    }
    for(let o = 0; o < fields; o++){
        const i = Math.floor(o / 10);
        const j = o % 10;
        let num = 0;
        const obj = document.getElementById(i + '_' + j);
        for(let k = 0; k < 9; k++){
            num += cl((i - (Math.floor(k / 3) -1)) + '_' + (j - (k % 3 - 1)));
        }
        obj.innerHTML = num === 0 ? '&nbsp;' : num;
        obj.onclick = function(){
            const mix = this.id.split('_');
            newGame = true;
            open(mix[0], mix[1]);
        };
        obj.oncontextmenu = function(){
            if(this.className.indexOf('flag') !== -1){
                this.className = this.className.replace(/ flag/, '');
                flag--;
                document.getElementById('text').innerHTML =  flag + '/' + bombs;
            }else {
                this.className = this.className + ' flag';
                flag++;
                document.getElementById('text').innerHTML =  flag + '/' + bombs;
            }
            return false;
        };

    }
    /**
     * Opening a cell field:
     *  - creating a button to start over
     *  - check cell on the bomb
     * @param {number} i
     * @param {number} j
     */
    function open(i, j){
        const dom = document.getElementById(i + '_' + j);
        if(newGame){
            const divNewGame = document.getElementById('btn');
            divNewGame.className = 'btn-new-game';
            divNewGame.innerText = 'Новая игра';
            divNewGame.style.display = 'block';
        }
        if(!dom || dom.className.indexOf('close') === -1){
            return;
        }
        if(dom.className.indexOf('bomb') !== -1){
            const divs = document.getElementsByTagName('div');
            for(i = 0; i < divs.length; i++){
                if(divs[i].className.indexOf('bomb')!== -1){
                    divs[i].className = 'bomb';
                }else{
                    divs[i].className = '';
                }
            }
            alert('You lose!');
        }
        else {
            dom.className='';
            const elems = document.getElementsByTagName('div');
            let len = 0;
            for (let ki in elems){
                if(elems[ki].className && elems[ki].className.indexOf('close')!== -1){
                    len++;
                }
            }
            if(len <= bombs){
                alert('You win!');
            }
        }
        if(dom.innerHTML === '&nbsp;'){
            for(let ks = 0; ks < 9; ks++){
                open(i - ((Math.floor(ks / 3) -1)), j - (((ks % 3) -1)));
            }
        }
    }
}
startGame(10,100);
