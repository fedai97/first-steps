/**
 * @desc Recursive object cloning
 * @param {object} mainObj
 * @param {object} cloneObj
 */
function cloning(mainObj, cloneObj) {
    for (let prop in mainObj) {
        cloneObj[prop] = mainObj[prop];
        if (typeof(mainObj[prop]) === 'object') {
            cloning(mainObj[prop], cloneObj[prop]);
        }
    }
}

const myTVSeries = [
    {
        "title": {
            "rus": "Во все тяжкие",
            "eng": "Breaking Bad"
        },
        "year": {
            "release": 2008,
            "end": 2013
        },
        "genre": [
            "драма",
            "триллер",
            "криминал" ],
        "country": "США",
        "rating": {
            "imdb": 9.5,
            "kinopoisk": 8.9
        }
    },
    {
        "title": {
            "rus": "Как я встретил вашу маму",
            "eng": "How I Met Your Mother"
        },
        "year": {
            "release": 2005,
            "end": 2014
        },
        "genre": [
            "драма",
            "комедия"
        ],
        "country": "США",
        "rating": {
            "imdb": 8.3,
            "kinopoisk": 8.6
        }
    },
    {
        "title": {
            "rus": "Бесстыжие",
            "eng": "Shameless"
        },
        "year": {
            "release": 2011,
            "end": "..."
        },
        "genre": [
            "драма",
            "комедия"
        ],
        "country": "США",
        "rating": {
            "imdb": 8.7,
            "kinopoisk": 8.6
        }
    }
];
let newObj = [];

console.log(myTVSeries);
cloning(myTVSeries, newObj);
console.log(newObj);

