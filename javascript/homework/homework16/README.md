## Теоретический вопрос

1.  Напишите как вы понимаете рекурсию. Для чего она используется на практике?
    
### Вопрос 1

*   Это когда функция вызывает сама себя (как правило с другими аргументами).
*   Используется для того чтоб, упростить одну сложную задачу несколькими простыми.