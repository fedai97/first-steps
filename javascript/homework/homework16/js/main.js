function checkNumber(enteredNumber){
    if(isNaN(enteredNumber) || enteredNumber === '' || enteredNumber === undefined || enteredNumber < 0){
        let newNumber = prompt('Re-enter your number (the field must be filled and have int value)','');
        return checkNumber(newNumber);
    }else{
        return enteredNumber;
    }
}
function factorial(num){
    return num ? num * factorial(num - 1) : 1;
}
let number = checkNumber(prompt('Enter your number', ''));
alert(factorial(number));