/**
 * Transition to the next image
 */
function nextImg() {
    if(step < document.getElementsByClassName('image-to-show').length-1){
        step++;
        activeImg[0].nextElementSibling.className = 'image-to-show active';
        activeImg[0].className = 'image-to-show';
    }else{
        step = 0;
        allImg[allImg.length-1].className = 'image-to-show';
        allImg[0].className = 'image-to-show active';
    }
    timerId = setTimeout(nextImg, 10000);
}

const allImg = document.getElementsByClassName('image-to-show');
const activeImg = document.getElementsByClassName('image-to-show active');
const insertBtn = document.getElementById('div-for-btn');
let timerId = setTimeout(nextImg, 10000);
let step = 0;

insertBtn.innerHTML = `<button id="stop">Прекратить</button>
                       <button id="start">Возобновить показ</button>`;

document.getElementById('stop').addEventListener('click', function () {
   clearTimeout(timerId);
});
document.getElementById('start').addEventListener('click', function () {
    timerId = setTimeout(nextImg, 10000);
});

