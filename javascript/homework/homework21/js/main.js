function filterCollection(arrayToFilter, stringKeyWords, boolFlag, ...stringNameField) {
    stringKeyWords = stringKeyWords.split(' ');
    let breakF = false;

    return arrayToFilter.filter(item => {
        let result = false;
        if(!breakF){
            stringNameField.forEach(itemStringField => {
                if(itemStringField.includes('.')){
                    itemStringField = itemStringField.split('.');
                    if(item[itemStringField[0]][itemStringField[1]] !== undefined){
                        stringKeyWords.forEach(keyWords => {
                            if(item[itemStringField[0]][itemStringField[1]] === keyWords){
                                result = true;
                                if(!boolFlag){
                                    breakF = true;
                                }
                            }
                        });
                    }
                }
                if(item[itemStringField] !== undefined){
                    stringKeyWords.forEach(keyWords => {
                        if(item[itemStringField] === keyWords){
                            result = true;
                            if(!boolFlag){
                                breakF = true;
                            }
                        }
                    });
                }
            });
            return result;
        }
    });
}

const array = [
    {
        name: "Mazda",
        year: 2019,
        contentType: {
            name: '3'
        },
        locales: {
            name: 'en_US'
        }
    }, {
        name: "BMW",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'UA'
        }
    },{
        name: "Audi",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'en_US'
        }
    },{
        name: "Lexus",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'UA'
        }
    },{
        name: "Toyota",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'en_US'
        }
    },{
        name: "LADA",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'UA'
        }
    },{
        name: "Test",
        year: 2017,
        contentType: {
            name: 'i7'
        },
        locales: {
            name: 'test'
        }
    }];
const keys = 'en_US Mazda LADA';
const flag = false;
const result = filterCollection(array, keys, flag, 'description', 'name', 'contentType.name', 'locales.name', 'locales.description');
console.log(result);