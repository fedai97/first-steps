/**
 * @desc Filter array for the specified data type.
 * @param {object} arr
 * @param {string} type
 * @return {*}
 */
function filterBy(arr, type){
    return arr.filter(item => typeof item !== type);
}

let arr = ['test', 1, null, 'world', 2, 3, true, 'hello' , undefined, { name: "Fedia" }, function () {}];
console.log(filterBy(arr, 'string'));