/**
 * Check, if developers have time to complete work before deadline.
 * @param {Number} day1
 * @param {Number} day2
 */
function checkProgress(day1, day2) {
    if(day1 - day2 < 0){
        const extraHours = -(day1 - day2) * 8;
        alert(`Команде разработчиков придется потратить дополнительно ${extraHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    }else{
        const restOfDays = day1 - day2;
        alert(`Все задачи будут успешно выполнены за ${restOfDays} дней до наступления дедлайна!`);
    }
}

/**
 * Workflow analysis:
 *  - how much work is done per day
 *  - how much work needs to be done
 *  - how many days are needed to do the work
 * @param arrayWorkSpeed
 * @param arrayBacklog
 * @param {Object} date
 */
function workflowAnalysis(arrayWorkSpeed, arrayBacklog, date) {
    const daysToDeadline = (date) => ((date - new Date())/1000/60/60/24).toFixed(0);
    const teamWorkSpeedPerDay = arrayWorkSpeed.reduce((previousValue, currentValue) => previousValue + currentValue);
    const countStoryPoint = arrayBacklog.reduce((previousValue, currentValue) => previousValue + currentValue);
    const dayNeedToEndWork = Math.ceil(countStoryPoint / teamWorkSpeedPerDay);
    checkProgress(daysToDeadline(date), dayNeedToEndWork);
}

const arrWorkSpeed = [2, 1, 6, 4, 2, 2];
const arrBacklog = [10, 5, 24, 12, 15, 10];
const deadlineDate = new Date("10/21/2019");
workflowAnalysis(arrWorkSpeed, arrBacklog, deadlineDate);