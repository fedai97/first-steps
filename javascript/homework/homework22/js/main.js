/**
 * Generation of random numbers in a certain range
 * @param {Number} min
 * @param {Number} max
 * @return {Number}
 */
function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Drawing circle for a given diameter.
 * @param {Number} diam
 * @param {Number} i
 */
function drawCircle(diam, i) {
    const contentDiv = document.getElementById('content');
    contentDiv.innerHTML += `<div class="rainbowCircle" id="list_circles${i}"></div>`;

    const arrColor = ['red', 'blue', 'green', 'yellow', 'black'];
    const newDiv = document.querySelector(`#list_circles${i}`);
    newDiv.style.cssText = `border-radius: 100%;
                            float: left;
                            margin: 10px;
                            background: ${arrColor[getRandomInRange(0, 4)]};
                            width: ${diam}px;
                            height: ${diam}px;`;
}
/**
 * Circle deletion handler (delegation).
 */
function delCircle() {
    const content = document.getElementById('content');
    content.addEventListener('click', function (event) {
        const target = event.target;
        if(target.className === 'rainbowCircle'){
            const delDiv = document.getElementById(`${target.id}`);
            delDiv.parentNode.removeChild(delDiv);
        }
    });
}
/**
 * Creating <div> to draw a circle.
 * @param {Number} diam
 */
function createCircle(diam) {
    const delDiv = document.getElementById('content');
    delDiv.parentNode.removeChild(delDiv);

    document.body.innerHTML = `<div id="content"></div>`;
    for(let i = 0; i<100; i++){
        drawCircle(diam, i);
    }
    delCircle();
}

/**
 * Receive diameter from user.
 */
function getDiameter(){
    const diameter = document.getElementById('diameter').value;
    if(diameter === ''){
        alert('Entered diameter.');
    }else {
        createCircle(diameter);
    }
}

/**
 * Handler at the click of button to start drawing a circle.
 */
btn_start.addEventListener('click', () => {
    const btn = document.getElementById('btn_start');
    btn.parentNode.removeChild(btn);

    document.body.innerHTML = `<div id="content"><input type="number" id="diameter"><button id="btn_create_circle" onclick="getDiameter()">Нарисовать</button></div>`;
});


