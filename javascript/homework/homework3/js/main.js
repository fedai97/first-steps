/**
 * @desc Check, if the entered parameter is a positive number.
 * @param value
 * @returns {boolean}
 */
function isPositiveInt(value) {
    return value % 1 === 0 && value >= 0;
}

/**
 * @desc Check, if the entered parameter is a number:
 * If not, call this function again.
 * @param value
 * @returns {number}
 */
function checkNumber(value) {
    if (!isPositiveInt(value) || isNaN(value) || value === '' || value === undefined) {
        let newNum = prompt('Re-enter your number(the field must be filled and have int value)', '');
        return checkNumber(newNum);
    } else {
        return value;
    }
}

/**
 * @desc Check if the entered parameter is a specific character (+,-,*,/):
 * If not, call this function again.
 * @param {string} operation
 * @returns {string}
 */
function checkOperation(operation) {
    if(operation === "+" || operation === "-" || operation === "*" || operation === "/"){
        return operation;
    }else{
        let newOperation = prompt('Re-enter your operation (must be: + - / *)', '');
        return checkOperation(newOperation);
    }
}

/**
 * @desc Summation function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function sum(value1, value2){
    return +value1 + +value2;
}
/**
 * @desc Subtract function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function minus(value1, value2){
    return +value1 - +value2;
}
/**
 * @desc Multiplication function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function multiply(value1, value2){
    return +value1 * +value2;
}
/**
 * @desc Division function
 * @param {number} value1
 * @param {number} value2
 * @returns {number}
 */
function division(value1, value2){
    return +value1 / +value2;
}
/**
 * @desc Check, what operation needs to be performed.
 * @param {number} value1
 * @param {number} value2
 * @param {string} operation
 */
function mainOperation(value1, value2, operation) {
    let result;
    if (operation === "+") {
        result = sum(value1, value2);
    } else if (operation === "-") {
        result = minus(value1, value2);
    } else if (operation === "*") {
        result = multiply(value1, value2);
    } else {
        result = division(value1, value2);
    }
    console.log(value1 + " " + operation + " " + value2 + " = " + result);
}

let firstNum = checkNumber(prompt('Pls, enter first number', ''));
let secondNum = checkNumber(prompt('Pls, enter second number', ''));
let operation = checkOperation(prompt('Pls, enter operation (+ - * /)', ''));
mainOperation(firstNum, secondNum, operation);