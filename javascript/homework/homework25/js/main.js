/**
 * Go to next slide.
 */
function plusSlide() {
    showSlides(slideIndex += 1);
}

/**
 * Go to previous slide.
 */
function minusSlide() {
    showSlides(slideIndex -= 1);
}

/**
 * Slide show.
 * @param {number} n
 */
function showSlides(n) {
    const slides = document.getElementsByClassName("item");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
}

let slideIndex = 1;
showSlides(slideIndex);
