function addSubject(tabel) {
    const subject = prompt('Enter subject name: ', '');
    const grade = prompt(`Enter ${subject} grade: `, '');
    if(subject !== null || grade !== null){
        tabel[subject] = grade;
        addSubject(tabel);
    }
    return tabel;
}
function getTabel(){
    let newTabel = {};
    return addSubject(newTabel);
}
function checkProgress(student){
    let numBadGrad = 0;
    let countSubj = 0;
    let sumGrad = 0;

    for (let subject in student.tabel) {
        countSubj++;
        sumGrad = sumGrad + +student.tabel[subject];
        if(+student.tabel[subject] < 4){
            numBadGrad++;
        }
    }

    if (numBadGrad === 0) {
        alert(`Студент ${student.name} ${student.lastname} переведен на следующий курс.`);
        if(sumGrad/countSubj){
            alert(`Студенту ${student.name} ${student.lastname} назначена стипендия`)
        }
    }
    console.log(countSubj);
    console.log(sumGrad);
}
function createStudent(name, lastname){
    const student = {};
    student.name = name;
    student.lastname = lastname;
    student.tabel = getTabel();
    checkProgress(student);
}

const enterName = prompt("Enter student name: ",'');
const enterLastname = prompt("Enter student lastname: ",'');
createStudent(enterName, enterLastname);