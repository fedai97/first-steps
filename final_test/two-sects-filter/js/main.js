$(document).ready(function(){
    function arrayFilter(firstArray, secondArray){
        return firstArray.filter(itemFirstArr => {
            let flag = true;
            secondArray.forEach(itemSecondArr => {
                if(itemFirstArr.name === itemSecondArr.name){
                    if(itemFirstArr.lastName === itemSecondArr.lastName){
                        flag = false;
                    }
                }
            });
            return flag;
        });
    }


    let dogsFavor = [{
        name: "Мария",
        lastName: "Салтыкова",
        age: 25
    }, {
        name: "Осана",
        lastName: "Меньшинова",
        age: 20
    }, {
        name: "Андрей",
        lastName: "Первозваный",
        age: 100
    }, {
        name: "Василий",
        lastName: "Гофман",
        age: 40
    }, {
        name: "Поручик",
        lastName: "Ржевский",
        age: "вечно молодой"
    }];

    let catsFavor = [{
        name: "Мария",
        lastName: "Розгозина",
        age: 22
    }, {
        name: "Осана",
        lastName: "Меньшинова",
        age: 20
    }, {
        name: "Андрей",
        lastName: "Первозваный",
        age: 100
    }, {
        name: "Алексей",
        lastName: "Гофман",
        age: 40
    }, {
        name: "Капитан",
        lastName: "Очевидность",
        age: "вечно молодой"
    }];

    const peopleInHeaven = arrayFilter(dogsFavor, catsFavor);
    console.log(peopleInHeaven);
});