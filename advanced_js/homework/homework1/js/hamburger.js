/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    if (size === undefined) {
        throw new HamburgerException('No size given');
    } else if (stuffing === undefined) {
        throw new HamburgerException('No stuffing given');
    } else if (size.split('_')[0] !== 'SIZE') {
        throw new HamburgerException(`Invalid size: '${size}'`);
    } else if (stuffing.split('_')[0] !== 'STUFFING') {
        throw new HamburgerException(`Invalid stuffing: '${stuffing}'`);
    } else if (typeof size !== 'string') {
        throw new HamburgerException(`Invalid size: 'Not a string'`);
    } else if (typeof stuffing !== 'string') {
        throw new HamburgerException(`Invalid stuffing: 'Not a string'`);
    } else {
        this._size = size;
        this._stuffing = stuffing;
        this._topping = [];
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

/**
 * Получение данных о гамбургере (цена и калорийность).
 * @return {Object}
 */
Hamburger.prototype.getData = function () {
    return {
        "SIZE_SMALL": {"price": 50, "calories": 20},
        "SIZE_LARGE": {"price": 100, "calories": 40},
        "STUFFING_CHEESE": {"price": 10, "calories": 20},
        "STUFFING_SALAD": {"price": 20, "calories": 5},
        "STUFFING_POTATO": {"price": 15, "calories": 10},
        "TOPPING_MAYO": {"price": 20, "calories": 5},
        "TOPPING_SPICE": {"price": 15, "calories": 0},
    }
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (topping === undefined) {
        throw new HamburgerException(`No topping given`);
    } else if (topping.split('_')[0] !== 'TOPPING') {
        throw new HamburgerException(`Invalid topping: '${topping}'`);
    } else {
        if (this._topping.indexOf(topping) !== -1) {
            throw new HamburgerException(`Duplicate topping: '${topping}'`);
        } else {
            this._topping.push(topping);
        }
    }
};
/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (topping === undefined) {
        throw new HamburgerException(`No topping given`);
    } else if (topping.split('_')[0] !== 'TOPPING') {
        throw new HamburgerException(`Invalid topping: '${topping}'`);
    } else {
        if (this._topping.indexOf(topping) !== -1) {
            this._topping.splice(this._topping.indexOf(topping), 1);
        } else {
            throw new HamburgerException(`No duplicate topping: '${topping}'`);
        }
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this._topping;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this._size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this._stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var hamburgerData = this.getData(),
        sizePrice = hamburgerData[this._size].price,
        stuffingPrice = hamburgerData[this._stuffing].price,
        toppingPrice = 0;

    if (this._topping.length > 0) {
        this._topping.forEach(item => toppingPrice += hamburgerData[item].price);
    }

    return sizePrice + stuffingPrice + toppingPrice;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var hamburgerData = this.getData(),
        sizeCalories = hamburgerData[this._size].calories,
        stuffingCalories = hamburgerData[this._stuffing].calories,
        toppingCalories = 0;

    if (this._topping.length > 0) {
        this._topping.forEach(item => toppingCalories += hamburgerData[item].calories);
    }

    return sizeCalories + stuffingCalories + toppingCalories;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.name = 'HamburgerException';
    this.message = message;
}

try {
    var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    console.log("Calories: %f", hamburger.calculateCalories());
    console.log("Price: %f", hamburger.calculatePrice());
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1

    // не передали обязательные параметры
    // var h2 = new Hamburger();
    // => HamburgerException: no size given

    // передаем некорректные значения, добавку вместо размера
    // var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
    // => HamburgerException: invalid size 'TOPPING_SPICE'

    // добавляем много добавок
    // var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // HamburgerException: duplicate topping 'TOPPING_MAYO'

} catch (e) {
    alert(`${e.name}: ${e.message}`);
}

