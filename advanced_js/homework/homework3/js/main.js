'use strict';

class Table {
    #col;
    #row;
    constructor(row, col) {
        this.#row = row;
        this.#col = col;
    }
    createTable() {
        const table = document.createElement('table');
        const tbody = document.createElement('tbody');

        for (let i = 1; i <= this.#row; i++) {
            let cols = '';
            for (let i = 1; i <= this.#col; i++) {
                cols += `<th class="col${i} white"></th>`;
            }
            tbody.innerHTML += `<tr class="row${i}">${cols}</tr>`;
        }
        table.appendChild(tbody);
        document.body.appendChild(table);
    }
    static changeThBackgroundColor(target){
        target.classList.toggle('active');
    }
    static revertAllBackgroundColor(target){
        const tables = target.getElementsByTagName('table');
        for(let table of tables){
            table.classList.toggle('active');
        }
    }
}

const table1 = new Table(30, 30);
table1.createTable();
// const table2 = new Table(10, 10);
// table2.createTable();

document.body.addEventListener('click', function (event) {
    const target = event.target;
    if(target === this){
        Table.revertAllBackgroundColor(target);
    }else{
        Table.changeThBackgroundColor(target);
    }
});