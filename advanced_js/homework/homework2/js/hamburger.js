'use strict';

class HamburgerException {
    constructor(message) {
        this.name = 'HamburgerException';
        this.message = message;
    }
}

class Hamburger {
    #size;
    #stuffing;
    #topping;
    constructor(size, stuffing) {
        if (size === undefined) {
            throw new HamburgerException('No size given');
        } else if (stuffing === undefined) {
            throw new HamburgerException('No stuffing given');
        } else if (size.split('_')[0] !== 'SIZE') {
            throw new HamburgerException(`Invalid size: '${size}'`);
        } else if (stuffing.split('_')[0] !== 'STUFFING') {
            throw new HamburgerException(`Invalid stuffing: '${stuffing}'`);
        } else if (typeof size !== 'string') {
            throw new HamburgerException(`Invalid size: 'Not a string'`);
        } else if (typeof stuffing !== 'string') {
            throw new HamburgerException(`Invalid stuffing: 'Not a string'`);
        } else {
            this.#size = size;
            this.#stuffing = stuffing;
            this.#topping = [];
        }
    }
    get toppings() {
        return this.#topping;
    };
    get size() {
        return this.#size;
    };
    get stuffing() {
        return this.#stuffing;
    };
    static getData() {
        return {
            "SIZE_SMALL": {"price": 50, "calories": 20},
            "SIZE_LARGE": {"price": 100, "calories": 40},
            "STUFFING_CHEESE": {"price": 10, "calories": 20},
            "STUFFING_SALAD": {"price": 20, "calories": 5},
            "STUFFING_POTATO": {"price": 15, "calories": 10},
            "TOPPING_MAYO": {"price": 20, "calories": 5},
            "TOPPING_SPICE": {"price": 15, "calories": 0},
        }
    }
    set addTopping(topping) {
        if (topping === undefined) {
            throw new HamburgerException(`No topping given`);
        } else if (topping.split('_')[0] !== 'TOPPING') {
            throw new HamburgerException(`Invalid topping: '${topping}'`);
        } else {
            if (this.#topping.indexOf(topping) !== -1) {
                throw new HamburgerException(`Duplicate topping: '${topping}'`);
            } else {
                this.#topping.push(topping);
            }
        }
    }
    set removeTopping(topping) {
        if (topping === undefined) {
            throw new HamburgerException(`No topping given`);
        } else if (topping.split('_')[0] !== 'TOPPING') {
            throw new HamburgerException(`Invalid topping: '${topping}'`);
        } else {
            if (this.#topping.indexOf(topping) !== -1) {
                this.#topping.splice(this.#topping.indexOf(topping), 1);
            } else {
                throw new HamburgerException(`No duplicate topping: '${topping}'`);
            }
        }
    }
    get price() {
        const hamburgerData = Hamburger.getData(),
            sizePrice = hamburgerData[this.#size].price,
            stuffingPrice = hamburgerData[this.#stuffing].price;
        let toppingPrice = 0;

        if (this.#topping.length > 0) {
            this.#topping.forEach(item => toppingPrice += hamburgerData[item].price);
        }

        return sizePrice + stuffingPrice + toppingPrice;
    };
    get calories() {
        const hamburgerData = Hamburger.getData(),
            sizeCalories = hamburgerData[this.#size].calories,
            stuffingCalories = hamburgerData[this.#stuffing].calories;
        let toppingCalories = 0;

        if (this.#topping.length > 0) {
            this.#topping.forEach(item => toppingCalories += hamburgerData[item].calories);
        }

        return sizeCalories + stuffingCalories + toppingCalories;
    };

}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

try {
    const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    console.dir(hamburger);
    hamburger.addTopping = Hamburger.TOPPING_MAYO;
    console.log("Calories: %f", hamburger.calories);
    console.log("Price: %f", hamburger.price);
    hamburger.addTopping = Hamburger.TOPPING_SPICE;
    console.log("Price with sauce: %f", hamburger.price);
    console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE);
    hamburger.removeTopping = Hamburger.TOPPING_SPICE;
    console.log("Have %d toppings", hamburger.toppings.length);
} catch (e) {
    alert(`${e.name}: ${e.message}`);
}

