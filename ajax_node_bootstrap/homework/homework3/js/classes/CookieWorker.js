class CookieWorker {
    static getCookie(name) {
        let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    static setCookie(name, value, options = {}) {
        options = {
            path: '/',
            ...options
        };
        let updatedCookie = name + '=' + value;
        for (let optionKey in options) {
            updatedCookie += "; " + optionKey + '=' + options[optionKey];
        }
        document.cookie = updatedCookie;
    }
}