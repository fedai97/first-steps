class FilmItem {
    constructor({episode_id, title, opening_crawl}) {
        this.episodeId = episode_id;
        this.title = title;
        this.openingCrawl = opening_crawl;
        this.characters = [];
    }
    set addCharacters(newCharacter){
        this.characters.push({characterId: newCharacter});
    }
}