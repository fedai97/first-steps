class CharacterItem{
    constructor({name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, url}) {
        this.id = url.split('people/')[1].split('/')[0];
        this.name = name;
        this.height = height;
        this.mass = mass;
        this.hairColor = hair_color;
        this.skinColor = skin_color;
        this.eyeColor = eye_color;
        this.birthYear = birth_year;
        this.gender = gender;
    }
}