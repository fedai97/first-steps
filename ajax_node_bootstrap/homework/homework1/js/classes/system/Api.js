class Api {
    constructor() {
        this.server = 'https://swapi.co/api';
    }
    getUrl(url) {
        return `${this.server}${url}`;
    }
    static parseResponse(response) {
        let data = response;
        const result = {
            status: false,
            data: [],
            message: ''
        };
        try {
            if (typeof response === 'string') {
                data = JSON.parse(response);
            }
            result.status = true;
            result.data = data;
        } catch (e) {
            result.message = e.message;
        }
        return result;
    }
    getByXHR(url, callback) {
        const request = new XMLHttpRequest();
        request.open('GET', this.getUrl(url));
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {
                if (typeof callback === 'function') {
                    callback(Api.parseResponse(request.response));
                }
            }
        };
        request.send();
    }
    getByAxios(url) {
        return axios(this.getUrl(url), {
            type: 'GET'
        });
    }

    getByJQuery(url, callback) {
        $.ajax(this.getUrl(url), {type: 'GET'})
            .done(response => {
                if (typeof callback === 'function') {
                    callback(Api.parseResponse(response));
                }
            })
            .error(error => {
                callback({
                    status: false,
                    data: [],
                    message: error.statusText
                });
            });
    }
}