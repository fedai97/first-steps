class DOM {
    static createFilmList(data){
        const ul = document.querySelector('ul');
        const newLi = document.createElement('li');
        const newUl = document.createElement('ul');
        const characterEmptyList = data.characters.map(item=> {
            return `<li class="no-active character-id-${item.characterId}"></li>`;
        }).join('');

        newUl.innerHTML = `<span>Characters:</span>${characterEmptyList}<br>`;

        newLi.innerHTML = `<span>"${data.title}" - Episode ${data.episodeId}</span><p>${data.openingCrawl}</p>`;
        newLi.appendChild(newUl);
        ul.appendChild(newLi);
    }
    static createCharacterList(data){
        const liCharacters = document.querySelectorAll(`li.character-id-${data.id}`);
        for (let liCharacter of liCharacters){
            liCharacter.classList.remove('no-active');
            liCharacter.classList.add('active');
            liCharacter.innerHTML = `${data.name}`;
        }
    }
    static clearUl(){
        const ul = document.querySelector('.content > ul');
        ul.innerHTML = '';
    }
}