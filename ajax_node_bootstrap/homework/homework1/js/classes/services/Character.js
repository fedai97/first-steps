class Character extends Api {
    getCharacterByXHR(character_id, callback) {
        super.getByXHR(`/people/${character_id}/`, callback);
    }
    getCharacterByJQuery(character_id, callback) {
        super.getByJQuery(`/people/${character_id}/`, callback);
    }
    getCharacterByAxios(character_id) {
        return super.getByAxios(`/people/${character_id}/`);
    }
}