class Films extends Api {
    getFilmsByXHR(callback) {
        super.getByXHR('/films/', callback);
    }
    getFilmsByJQuery(callback) {
        super.getByJQuery('/films/', callback);
    }
    getFilmsByAxios() {
        return super.getByAxios('/films/');
    }
}