document.addEventListener('DOMContentLoaded', function () {
    const buttonByXHR = document.getElementById('btnGetDataByXHR');
    const buttonByAxios = document.getElementById('btnGetDataByAxios');
    const buttonByJQuery = document.getElementById('btnGetDataByJQuery');

    buttonByXHR.addEventListener('click', function () {
        const films = new Films();
        DOM.clearUl();
        films.getFilmsByXHR(onFilmListHandler);
    });
    buttonByJQuery.addEventListener('click', function () {
        const films = new Films();
        DOM.clearUl();
        films.getFilmsByJQuery(onFilmListHandler);
    });
    buttonByAxios.addEventListener('click', function () {
        const arrayPromiseCharacters = [];
        const arrayUnicCharaptersID = [];
        const films = new Films();
        DOM.clearUl();
        films.getFilmsByAxios()
            .then(result => {
                result.data.results.forEach(item => {
                    const film = new FilmItem(item);
                    item.characters.forEach(characterApi => {
                        const characterId = characterApi.split('people/')[1].split('/')[0];
                        film.addCharacters = characterId;
                        if(arrayUnicCharaptersID.indexOf(characterId) === -1){
                            arrayUnicCharaptersID.push(characterId);
                            const character = new Character();
                            arrayPromiseCharacters.push(character.getCharacterByAxios(characterId));
                        }
                    });
                    DOM.createFilmList(film);
                });
            })
            .then(result => {
                Promise.all(arrayPromiseCharacters).then(result => {
                    result.forEach(item => {
                        const character = new CharacterItem(item.data);
                        DOM.createCharacterList(character);
                    });
                });
            })
            .catch(console.error);
    });
});

function onFilmListHandler(response) {
    if (response && response.status === true && Array.isArray(response.data.results)) {
        const arrayUnicCharaptersID = [];
        response.data.results.forEach(result => {
            const film = new FilmItem(result);
            result.characters.forEach(characterApi => {
                const characterId = characterApi.split('people/')[1].split('/')[0];
                film.addCharacters = characterId;
                if(arrayUnicCharaptersID.indexOf(characterId) === -1){
                    const character = new Character();
                    arrayUnicCharaptersID.push(characterId);
                    character.getCharacterByXHR(characterId, onCharacterHandler);
                }
            });
            DOM.createFilmList(film);
        });
    } else if (response && response.status === false) {
        alert(response.message);
    }
}
function onCharacterHandler(response) {
    if (response && response.status === true) {
        const character = new CharacterItem(response.data);
        DOM.createCharacterList(character)
    } else if (response && response.status === false) {
        alert(response.message);
    }
}