class IPData extends Api {
    getIpifyByAxios() {
        return super.getByAxios('https://api.ipify.org/?format=json');
    }
    getDataApiByAxios(ip) {
        return super.getByAxios(`http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,regionName,city,district`);
    }
}