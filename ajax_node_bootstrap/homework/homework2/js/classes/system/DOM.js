class DOM {
    static addInfoToDOM(data){
        const divContent = document.querySelector('.content');
        for (let i in data){
            if(data[i].length > 0){
                divContent.innerHTML += `<p><span class="first">${i}: </span><span class="second">${data[i]}</span></p>`;
            }
        }
    }
}