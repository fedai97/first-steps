document.addEventListener('DOMContentLoaded', function () {
    const buttonGetIP = document.getElementById('btnGetIP');
    buttonGetIP.addEventListener('click', function () {
        findByIP();
    });
});

async function findByIP() {
    try {
        const getIP = new IPData();
        const ip = await getIP.getIpifyByAxios();
        const infoByIP = await getIP.getDataApiByAxios(ip.data.ip);
        DOM.addInfoToDOM(infoByIP.data);
    } catch (e) {
        console.error(e);
    }
}
