document.addEventListener('DOMContentLoaded', function() {
    const button = document.getElementById('btnGetData');
    button.addEventListener('click', function() {
        // const films = new Data();
        const clients = new Data();

        clients
            .getByPromiseByJQ()
            .then(function (result){
                result.data.data.forEach(item => {
                    console.log(`${item.firstName} ${item.lastName}`);
                });
            })
            .catch(error => {
                console.error('Error--> ', error);
            })
            .finally(() => {
                console.log('The End.');
            });


        // films.getFilms(onFilmListHandler);
    });
});

function onFilmListHandler(response) {
    const ul = document.querySelector('ul');

    if (response && response.status === true && Array.isArray(response.data.data)) {
        response.data.data.forEach(result => {
            const li = document.createElement('li');
            li.innerHTML = `${result.firstName} ${result.lastName} ${result.birthDate}`;
            ul.appendChild(li);
        });
    } else if (response && response.status === false) {
        alert(response.message);
    }
}
