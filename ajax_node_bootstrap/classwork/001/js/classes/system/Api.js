class Api {
    constructor() {
        this.server = 'http://192.168.12.126:3000';
    }

    getUrl(url) {
        return `${this.server}${url}`;
    }

    static parseResponse(response) {
        let data = response;
        const result = {
            status: false,
            data: [],
            message: ''
        };
        try {
            if (typeof response === 'string') {
                data = JSON.parse(response);
            }
            result.status = true;
            result.data = data;
        } catch (e) {
            result.message = e.message;
        }
        return result;
    }

    get(url, callback) {
        const request = new XMLHttpRequest();
        request.open('GET', this.getUrl(url));
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {
                if (typeof callback === 'function') {
                    callback(Api.parseResponse(request.response));
                }
            }
        };
        request.send();
    }

    getByPromise(url) {
        const self = this;
        return new Promise((resolve, reject) => {
            $.ajax(self.getUrl(url), {type: 'GET'})
                .done(response => {
                    resolve(Api.parseResponse(response));
                })
                .error(error => {
                    reject(error);
                });
        });
    }
}