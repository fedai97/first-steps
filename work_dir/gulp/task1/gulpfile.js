var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
    gulp.src('./proj/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./proj'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./proj/style.scss', ['sass']);
});